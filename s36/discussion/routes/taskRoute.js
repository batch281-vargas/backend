// Contains all the endpoints for our applications
// We seperate the routes such that "app.js" only contains information on the server
// We need to use express' Router() function top to achieve this

const express = require("express");
//Creates a Router intance that functions as a middleware and routhing system


//Allows access to HTTP method middlewars that make it easier to create routes for our application

const router = express.Router();

const taskController = require('../controllers/taskController');

// [Section] Routes
// The routes are responsible for define thE URI that our client access and the corresponding controlling functions that will be used when a route is accessed.
// They invije the controller function from the controller files
// All the busness logic is done in the controller

// Route is to get all the tasks
// This route expects to receive a Get request at the URL "/tasks"
// THe whole URL is at "http://localhost:4000/tasks"

router.get("/", (req, res) => {

	// Invoke the "getALltasks function from "taskController.js"
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));

})


// Route to create a new task
// This route expects recieve a POST request at the URL "tasks"
// The whole URL is at "htpp: localhost 4000 tasks"

router.post("/", (req, res)=> {
	// createTasks

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController)); 

})


// DELETE
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


// UPDATE

router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


// ACTIVITY Get Specific Tasks

router.get("/:id", (req, res) => {
	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// ACTIVITY Change the status Tasks

router.put("/:id/complete", (req, res) =>{
	taskController.changeStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// Object to export the router object to use in app.js
module.exports = router;