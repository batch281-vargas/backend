const mongoose = require("mongoose");


// Mongoose Schema
const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
});

//
module.exports = mongoose.model("Task", taskSchema);