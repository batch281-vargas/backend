// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection
// Connecting to MongoDB Atlas

mongoose.connect("mongodb+srv://nielvargas96:MbQPGFGeZkjPBnXt@wdc028-course-booking.wixug5i.mongodb.net/s36?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

// Add the tasks route
// Allows all task routes created in the "taskRoute.js" file to use "/tasks" route
// MONGO DATABSE NAEM IS tasks
app.use("/tasks", taskRoute)

// http:/localhost:4000/user/get

// Server listening
if(require.main === module) {
	app.listen(port, ()=> console.log(`Server is running ${port}`))
}

module.exports = app;

