// Controllers contains  functions and business logic of our express js applciation
// Meaning all the operations it can do will be placed in this file



const Task = require("../models/task");


// let getAllTasks = new Task()


module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return result;
	})
}


// Controller function for create a task
// The

module.exports.createTask = (requestBody) => {

	let newTask = new Task({

		// Sets the name property with the value received fromt he client/postman
		name: requestBody.name
	})


	// Save the newly tasks "newTask"

	return newTask.save().then((saveTasks, err) => {
		if(err) {
			console.log(err);
			return false
		} else {
			return saveTasks;
		}	
	})
}

// DELETE CONTROLER
module.exports.deleteTask = (taskId) => {
		// findByIdAndRemove native express method
	return Task.findByIdAndRemove(taskId).then((removeTask, err)=> {

		if(err) {
			console.log(err);
			return false;
		} else {
			return removeTask
		}

	})
}


// Controller function for updateing a tasks


module.exports.updateTask = (taskId, newContent) => {


	// findById
	return Task.findById(taskId).then((result, err) => {
		if(err) {
			console.log(err);
			return false;
		} 

		result.name = newContent.name;

		return result.save().then((updateTask, err)=>{

			if(err) {
				console.log(err);
				return false;
			} else {
				return updateTask;
			}
		})
	})	

}


// Acivity Controller for Getting a Specific Tasks


module.exports.getSpecificTask = (id) => {

	return Task.findById(id).then((result,err) => {

		if(err) {
			console.log(err);
			return false;
		} else {
			return result;
		}
	})

}


// Acivity For Change the Status of the Tasks


module.exports.changeStatus = (id, complete) => {
	return Task.findById(id).then((result, err) => {
		if(err) {
			console.log(err);
			return false;
		}

		result.status = "complete"

		return result.save().then((changeStatus, err) => {
			if(err) {
				console.log(err);
				return false
			} else {
				return changeStatus;
			}
		})
	})
}

















