const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");


// Create Product
router.post("/createProduct", auth.verify, (req, res) => {

	let myAuth = auth.decode(req.headers.authorization);
	
	const data = {
		product: req.body,
		isAdmin: myAuth.isAdmin
	}

	if(data.isAdmin == true){
        productController.addProduct(data)
            .then(result => res.send("Product added.."))
            .catch(error => res.send("Failed to add product!"));
	} else {
        res.send("Authorization failed!");
	}
});

// Retrieve all active products
router.get("/allAProducts", (req, res) => {
    productController.getAllProduct()
        .then(result => res.send(result))
        .catch(error => res.status(500).send({ error: "Failed to fetch active products" }));
});


// Retrieve all active products
router.get("/allActiveProducts", (req, res) => {
    productController.getAllActiveProduct()
        .then(result => res.send(result))
        .catch(error => res.status(500).send({ error: "Failed to fetch active products" }));
});


// Retrieve specific single product by its ID
router.get("/:productId", (req, res) => {
	// console.log(req.params.productId);
	productController.getSingleProduct(req.params).then(result => res.send(result));
})


// Updated product by Admin
router.put("/:productId/", auth.verify, (req, res) => {
	let myAuth = auth.decode(req.headers.authorization);

	const data = {
		product: req.body,
		isAdmin: myAuth.isAdmin
	}

	if(data.isAdmin == true){
		productController.updateProduct(req.params, data).then(result => res.send(`Product updated!`))
	} else {
			res.send(false);
		}
})


// Archive product by Admin
router.put("/:productId/archive", auth.verify, (req, res) => {
    let myAuth = auth.decode(req.headers.authorization);

    const data = {
        product: req.body,
        isAdmin: myAuth.isAdmin
    };

    if (data.isAdmin) {
        productController.archiveProduct(req.params.productId, data)
            .then(result => {
                if (result) {
                    res.send("Product archived!");
                } else {
                    res.send("Failed to archive product");
                }
            })
            .catch(error => {
                res.status(500).send({ error: "Failed to archive product" });
            });
    } else {
        res.send("Unauthorized to archive product");
    }
});



// Activate product by Admin
router.put("/:productId/activate", auth.verify, (req, res) => {
    let myAuth = auth.decode(req.headers.authorization);

    const data = {
        product: req.body,
        isAdmin: myAuth.isAdmin
    };

    if (data.isAdmin) {
        productController.activateProduct(req.params.productId, data)
            .then(result => {
                if (result) {
                    res.send("Product activated!");
                } else {
                    res.send("Failed to activate a product");
                }
            })
            .catch(error => {
                res.status(500).send({ error: "Failed to activate product" });
            });
    } else {
        res.send("Unauthorized to activate a product");
    }
});





module.exports = router;
