const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


// Register User
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
});

// Login User
router.get("/login", (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
});

// Retrieve User details
router.get("/userDetails", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	userController.retrieveUser({userId: userData.id}).then(result => res.send(result));
});

// Create Order
router.post("/:userId/order", (req, res) => {
	let data = {
		productId: req.body.productId
	}

	userController.order(req.params.userId, data).then(result => res.send(result));
});


module.exports = router;


