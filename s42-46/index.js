const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const productRoutes = require("./routes/productRoutes");	
const userRoutes = require("./routes/userRoutes");	

const app = express();

mongoose.connect("mongodb+srv://nielvargas96:MbQPGFGeZkjPBnXt@wdc028-course-booking.wixug5i.mongodb.net/capstone_2?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "MongoDB connection Error."));
db.once('open', ()=> console.log('Now connected to MongoDB Atlas!'));

app.use(cors());
app.use(express.json()); 
app.use(express.urlencoded({extended:true})); 


app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(process.env.PORT || 4000, () => {
	console.log(`Server is now online... on port ${process.env.PORT || 4000 }`);
})

