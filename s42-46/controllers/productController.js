const Product = require("../models/Product");

module.exports.addProduct = (data) => {

	let newProduct = new Product({
		name: data.product.name,
		description: data.product.description,
		price: data.product.price
	});

	return newProduct.save().then((product, err) => {
		if(product) {
			return true;
		} else {
			return false;
		}
	})
}

// All product
module.exports.getAllProduct = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

// All active product
module.exports.getAllActiveProduct = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}

// Single product
module.exports.getSingleProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

// Update Admin
module.exports.updateProduct = (reqParams, data) => {

	let updatedProduct = {
		name: data.product.name,
		description: data.product.description,
		price: data.product.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, err)=> {
		if (product) {
			return true;
		} else {
			return false;
		}
	})
}

// Archive Admin

module.exports.archiveProduct = (productId, data) => {
    const updatedData = {
        isActive: false
    };

    return Product.findByIdAndUpdate(productId, updatedData)
        .then(product => {
            if (product) {
                return true;
            } else {
                return false;
            }
        })
        .catch(error => {
            throw new Error("Failed to archive product");
        });
};


// Activate Product Admin

module.exports.activateProduct = (productId, data) => {
    const activatedProduct = {
        isActive: true
    };

    return Product.findByIdAndUpdate(productId, activatedProduct)
        .then(product => {
            if (product) {
                return true;
            } else {
                return false;
            }
        })
        .catch(error => {
            throw new Error("Failed to activate product");
        });
};



