const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, err) => {
		if(err) {
			return false;
		} else {
			
			return console.log(`New user created..`);
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then((result) => {

		// User does not exists
		if(result == null ) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result) }
			} else {
				return false;
			}
		}

	})
}

module.exports.retrieveUser = (data) => {

	return User.findById(data.userId).then(result => {
		result.password = "";

		return result;
	})
}

// Create order
module.exports.order = async (reqUserId, data) => {

	let isUserUpdated = await User.findById(reqUserId).then(user => {

		// user.orderedProduct.push({products.productId: data.productId})

		const newProduct = {
			productId: data.productId,
			productName: data.productName,
			quantity: data.quantity
		};

		const orderedProductIndex = user.orderedProduct.findIndex(op => op.products);
		if (orderedProductIndex !== -1) {
			user.orderedProduct[orderedProductIndex].products.push(newProduct);
		} else {
			user.orderedProduct.push({
				products: [newProduct]
			});
		}
		
		return user.save().then((user, err) => {
			if(err){
				return false;
			} else {
				return true;
			}
		})
	})


	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.usersOrder.push({userId : reqUserId});

		return product.save().then((product, err) => {
			if(err){
				return false;
			} else {
				return true;
			}
		});
	});


	if(isUserUpdated && isProductUpdated) {
		return true;
	} else {
		return false;
	}

}
