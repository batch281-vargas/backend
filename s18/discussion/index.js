
function printName(name) {
	console.log("My name is " + name);
}


printName("Hi");



//Functions as arguments

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log(`The remainder ${num} divided by 8 is: ${remainder}`)

	let isDivisibleBy8 = remainder === 0;
	console.log(`Is ${num} isDivisibleBy 8?`);
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);



function argumentFunction() {
	console.log('Displaying info here....')
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);


// Multiple parameters

function fullName(name,middle,lastname) {
	console.log(`My full name: ${name} ${middle} ${lastname} `)
}

fullName('Niel', 'Harisco', 'Vargas')

	


function returnFucntion(firstName, middleName, lastName) {
	return `${firstName} ${middleName} ${lastName}`;
	console.log('Hindi eto ma prirprint because hindi sya naka pasok sa return.')
}

//Need ito i store sa variable when accessing the return function
// returnFucntion('test', 'test', 'test')
let returnVariable = returnFucntion('Niel', 'Harisco', 'Vargas');
console.log(returnVariable)
console.log(returnFucntion('Niel', 'Harisco', 'Vargas'))


function addressFunction (city, country){
	let fullAddress = `${city}, ${country}`;
	return fullAddress;
}

let myAddress = addressFunction('Bacolod', 'Philippines');
console.log(myAddress);