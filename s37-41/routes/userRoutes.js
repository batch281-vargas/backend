const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");
// const jwt = require("jsonwebtoken");

// Route for checking the email exist in database
// Invokes the checkEmailExists function  from  the controller file
router.post("/checkEmail", (req, res)=> {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})


// Register USER
router.post("/register", (req, res)=> {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for details

// "authi.verify" acts as a middle ware to ensure that the user is logged in before then can enroll to a course
router.get("/details", auth.verify, (req, res) => {

	// uses decode method defined in the auth.js file toretrieve user information from the token passing the "token" from the request headder
	const userData = auth.decode(req.headers.authorization);


	// userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})

// Route to enroll user to a course

router.post("/enroll", auth.verify,(req, res) => {
	let myAuth = auth.decode(req.headers.authorization);


	let data = {
		userId: myAuth.id,
		isAdmin: myAuth.isAdmin,
		courseId: req.body.courseId
	}

	if(data.isAdmin == true){
    	userController.enroll(data).then(resultFromController => res.send(resultFromController))
	} else {
			res.send(false);
		}	
})


module.exports = router;

