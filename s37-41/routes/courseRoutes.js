const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for create a course



router.post("/", auth.verify, (req, res) => {

	// if(req.user.isAdmin) {
	// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	// } else {
	// 	return res.send(`Access denied. User msut be an admin.`)
	// }

	// const userData = auth.decode(req.headers.authorization);

	// if (userData.isAdmin) {
	// 		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	// 	} else {
	// 		res.send("Access denied. User must be an admin.");
	// 	}

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}

})


// Route for retrieving all the courses
router.get("/all", (req,res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})


// Route for regreiveing all the courses that isActive
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving specific courses
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})


router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})


router.put("/:courseId/", auth.verify, (req, res) => {
		const data = {
			course: req.body,
			isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
	courseController.archiveCourse(req.params, data).then(resultFromController => res.send(resultFromController))
	} else {
			res.send(false);
		}
})



module.exports = router;