const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		// Find method to returns record if a match is found
		if(result.length > 0 ) {
			return true
		} else {
			return false
		}
	})
}

// User registration
// HASHING BCRYPT 
// npm install bycrpyt


module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, err) => {
		if(err) {
			return false;
		} else {
			return true;
		}
	})
}



// USER LOGIIN AUTHENTICATION

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then((result) => {


		// User does not exists

		if(result == null ) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result) }
			} else {
				return false;
			}
		}

	})
}

// USER DETAILS


module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {
		result.password = "";

		return result;
	})
	// 	return User.findById(data.userId).then((result,err) => {
	// 	if(err) {
	// 		console.log(err);
	// 		return false;
	// 	} else {
	// 		return result;
	// 	}
	// })
}


// Enroll user to a class
// Async await will be used in enrolling the user
module.exports.enroll = async (data) => {


	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId})
		
		return user.save().then((user, err) => {
			if(err){
				return false;
			} else {
				return true;
			}
		})
	})


	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId : data.userId});

		return course.save().then((course, err) => {
			if(err){
				return false;
			} else {
				return true;
			}
		});
	});


	if(isUserUpdated && isCourseUpdated) {
		return true;
	} else {
		return false;
	}

	// if(isUserUpdated && isCourseUpdated) {
	// 	return true	
	// }

}





