const Course = require("../models/Course");


module.exports.addCourse = (data) => {

	// Create "newCourse" and instantiates a new "Course" object using mongoose model
	// let newCourse = new Course({
	// 	name: reqBody.name,
	// 	description: reqBody.description,
	// 	price: reqBody.price
	// })

	let newCourse = new Course({
		name : data.course.name,
		description : data.course.description,
		price : data.course.price
	});


	return newCourse.save().then((course, err) => {
		if(course) {
			return true;
		} else {
			  console.error("Error adding course:", err);
			return false;
		}
	})
}

// Retrieve all courses

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// Retrieving all the active course
module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	})
}

// Route for retrieving a specific course

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Update a course


module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, err)=> {
		if (err) {
			return false;
		} else {
			// course updated succesfully
			return true;
		}
	})
}




module.exports.archiveCourse = (reqParams, data) => {
	let archivedCourse = new Course({
		isActive : data.course.isActive
	})

	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, err)=> {
		if (err) {
			return false;
		} else {
			// course updated succesfully
			return true;
		}
	})
}
