let http = require("http");

// Mock datbase
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
];


let port = 4000;

let app = http.createServer((req, res)=> {
	if(req.url == "/users" && req.method == "GET") {
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.write(JSON.stringify(directory))
		res.end()
	}

	if(req.url ==  "/users" && req.method == "POST") {
		let requestBody = "";

		// Stream is a sequence of datbase
		req.on('data', (data)=> {
			// Assign that data retrieve from the data stream to requestBody
			requestBody += data;
		});

		req.on('end', ()=> {
			console.log(typeof requestBody);

			//coverts string requestbody to JOSN
			requestBody = JSON.parse(requestBody);

			//Create a new object representing a new mokcup database
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}

			// Add the new users to the mock database
			directory.push(newUser);
			console.log(directory)

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.write(JSON.stringify(newUser));
			res.end();
		})
	}
});

app.listen(port, ()=> {
	console.log('Server running at localhost:4000')
});

