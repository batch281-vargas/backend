let cp = {
	name: 'Nokia',
	manufactureDate: 1999,
};

console.log(cp);
console.log(typeof cp)

function Laptop(name, manufactureDate) {
	this.aname = name;
	this.manufactureDate = manufactureDate;
}

// console.log(laptop.a)

// Intances


let laptop = new Laptop('3310', 2001);
console.log(laptop)
console.log(laptop.a)
console.log(laptop.manufactureDate)


let myLaptop = new Laptop('Macos', 2002);
// console.log(laptop)
let array = [laptop, myLaptop];

console.log(array[1].aname)



let car = {}

car.name = 'Honda Civice';

console.log(car)



car['manufacture date'] = 2019;
console.log(car['manufacture date']);
console.log(car['Manufacture Date']);
console.log(car.manufactureDate);
console.log(car)

delete car['manufacture date'];
console.log(car)

car.name = 'Lamborghini';
console.log(car)


// Objects methods

let person = {
	name: 'John',
	talk: function (e) {
		console.log(`Hello my name is ${this.name} ${e}`)
	}
}

console.log(person);
person.talk('wow');


console.log('============');

// Adding methods to objects

person.walk = function (a) {
	console.log(`${this.name} ${a}`)
	// console.log(`${this.talk(a)}`)
}

person.walk('test params')


//Real world Games
console.log('============');


let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log(`This pokemon tackled targetPokemon`);
		console.log(`targetPokemon health is now reduce to _targetPokemonHealth_`)
	},
	faint: function () {
		console.log(`Pokemon fainted.`)
	}
}
console.log(myPokemon)
myPokemon.tackle();


console.log('============');
console.log('============');

// Creating an object constructor
function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;


	// Methods
	this.tackle = function(target) {
		console.log(`${this.name} tackle ${target.name}`)
		console.log(`targetPokemon health is reduce to _targetPokemonHealth_`)
	}

	this.faint = function () {
		console.log(``)
	}
}


// Create new instance of Pokemon object
let pikachu = new Pokemon('Pikachu', 16)
let ratata = new Pokemon('Ratata', 10)

pikachu.tackle(ratata)









