// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object



let trainer = {
    name: 'Ash Ketchum',
    friends: ['May', 'Max'],
    pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
    talk: function(pok) {
        console.log(`Result of talk method:`);
        console.log(`${this.pokemon[0]}! I choose you!`);
    }
}

console.log(trainer)
console.log(`Result of dot notation:\n${trainer.name}`)
console.log(`Result of square notation:\n${trainer['pokemon']}`)

trainer.talk();

function Pokemon (name, level) {
    this.name = name;
    this.level = level
    this.health = 2 * level;
    this.attack = level;

    // Methods
    this.tackle = function(target) {
        console.log(`-----------------------------------------`);
        console.log(`${this.name} tackle ${target.name}`);
        this.faint(target)
    }

    this.faint = function (target) {
        let totalTargetHealth = target.health - this.attack;
        console.log(`${target.name}'s health is reduce to ${totalTargetHealth}`)
        if(totalTargetHealth <= 0) {
            console.log(`${target.name} fainted`)
        } else {
            console.log(`${target.name} current health "${totalTargetHealth}" `)
        }
    }
}

let pikachu = new Pokemon('Pikachu', 16);
let mewtwo= new Pokemon('Mewtwo', 100);
let bulbasaur = new Pokemon('Bulbasaur ', 50);

console.log(pikachu);
console.log(mewtwo);
console.log(bulbasaur);

pikachu.tackle(mewtwo);
bulbasaur.tackle(mewtwo);
mewtwo.tackle(pikachu);



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}