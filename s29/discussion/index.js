// [Selectio] Comparisons Query Operators


//db.collectionName.find({field: {$lt: value}})

// $gt/gte

db.users.find({age: {$gt: 20}})
db.users.find({age: {$gte: 21}})


// $lt/lte less than or equal to lesthan

db.users.find({age: {$lt: 20}})
db.users.find({age: {$lte: 21}})

// $ne operator not equal
// Allows us to find documents that have field numbers not equal to specified value
//  db.dbCollectionName.find({field: {$lte: value}})

db.users.find({age: {$ne: 21}})


// $in operator

// Allows us to find documents with specific match criteria one field using different values

db.users.find({lastName: { $in: ['Dela Cruz', 'Gates'] }})

// $nin  Opposite on in

db.users.find({lastName: { $nin: [ 'Gates'] }})


// Logical operator

// $or operator 
// Allows us to find documents that match a single criteria from multiple provieded search

// Syntax db.dbCollectionName.find({$or : [{field: value}, {field: value}]});

db.users.find({$or : [{firstName: "Bill"}, {age: 21}]});

db.users.find({$or : [{firstName: "Bill"}, {age: {$gt: 0}}]});


// $and operator 

// allow us to find documents mathcing multiple criteria in a single field


db.users.find({$and: [ {age: {$ne: 21}}, {age: {$ne: 0}} ]})


// Field projection
// Retrieveing docments are common operations that we do and by defailt mongoDB queries return whole doc as a response



// Inclusion
// allow us to inlude specifc field only
//Syntax db.dbCollectionName.find({criteria, { field: 1}});
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
)



// Excusion
// allow us to remove specifc field only
//Syntax db.dbCollectionName.find({criteria, { field: 1}});
db.users.find(
	{
		firstName: "Jane"
	},
	{
		id: 0,
		contact: 0
	}
)


// Supressing ID
db.users.find(
	{
		firstName: "John"
	},
	{
		_id: 0,
		lastName: 1,
		firstName: 1,
		'contact': 1
	}
)


//// Supressing ID
db.users.find(
	{
		firstName: "John"
	},
	{
		lastName: 1,
		lastName: 1,
		'contact.phone': 1
	}
)


// Project specific array elements in returned.


db.users.insert({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }
    ]
});

//The slice operator allows us to specify the number of elements that we want to return from an array

db.users.find(
	{ "namearr": 
		{ 
			namea: "juan" 
		} 
	}, 
	{ namearr: 
		{ $slice: 1 } 
	}
)


// $regex EVALUATION QUERY

/*
	Allows us to find documents that match a specific string pattern using regular express
	db.users.find({field: $regex: 'pattern', $options: 'i'})
*/

// Case sensitive
db.users.find({firstName: { $regex: 'B' }})

// Case insensitive // $options: 'i' for insensitive
db.users.find({firstName: { $regex: 'a', $options: 'i' }})