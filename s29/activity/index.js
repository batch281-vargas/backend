
// #2
db.users.find({$or : [{firstName: {$regex: 's'}}, {lastName: {$regex: 'd'}}]},{_id: 0});

// #3
db.users.find({$and: [ {department: "HR"}, {age: {$gte: 70}} ]})


// #4
db.users.find({$and: [ {firstName: {$regex: 'e', $options: 'i'}}, {age: {$lte: 30}} ]})
