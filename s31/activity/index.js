//- What directive is used by Node.js in loading the modules it needs?
Require directive
//- What Node.js module contains a method for server creation?
http module
//- What is the method of the http object responsible for creating a server using Node.js?
http. createServer()
//- What method of the response object allows us to set status codes and content types?
status method
//- Where will console.log() output its contents when run in Node.js?
response.statusCode
//- What property of the request object contains the address's endpoint?
request.url


const http = require('http');


const server = http.createServer((req, res)=> {

	if(req.url == '/login') {
		res.writeHead(200, {'Content-Type': 'text/plain'});
	    res.end('Welcome to login page!');
	} else if (req.url == '/register') {
		res.writeHead(200, {'Content-Type': 'text/plain'});
	    res.end('The page you are looking for cannot be found.');
	} else {
		res.writeHead(200, {'Content-Type': 'text/plain'});
	    res.end('Page not available...');
	}
})

const port = 3000;
server.listen(port, ()=> {
	console.log(`Server running at http://localhost:${port}`);
})

