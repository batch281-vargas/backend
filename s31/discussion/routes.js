const http = require('http');


const server = http.createServer((req, res)=> {

	if(req.url == '/greeting') {
		res.writeHead(200, {'Content-Type': 'text/plain'});
	    res.end('Greeting!');
	} else if (req.url == '/homepage') {
		res.writeHead(200, {'Content-Type': 'text/plain'});
	    res.end('Homepage');
	} else {
		res.writeHead(200, {'Content-Type': 'text/plain'});
	    res.end('Page not available...');
	}
})

const port = 4000;
server.listen(port, ()=> {
	console.log(`Server running at http://localhost:${port}`);
})