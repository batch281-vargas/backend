const http = require('http');

// const server = http.createServer((req, res) => {
//   res.statusCode = 200;
//   res.setHeader('Content-Type', 'text/plain');
//   res.end('Hello, World!');
// });

// const port = 3000;
// server.listen(port, () => {
//   console.log(`Server running at http://localhost:${port}/`);
// });



http.createServer((req, res)=> {
  // Returning what type of response thrown to the client
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello, World!')
}).listen(4000)

console.log('Server is running');