function login(user, pass , role) {
	if(user == '' || user == undefined || pass == '' || pass == undefined || role == '' || role == undefined) {
		console.log( `Input should not be empty.`);
	} 
	else {
		switch(role) {
			case 'admin':
				console.log("Welcome back to the class portal, admin!")
				break;
			case 'teacher':
				console.log("Thank you for logging in, teacher!")
				break;
			case 'rookie':
				console.log("Welcome to the class portal, student!")
				break;
			default :
				console.log("Role out of range.")
				break;
		}
	}
}

login('user_1', 'pass_123', 'admin');

function checkAverage(num1, num2, num3, num4) {
	let totalAverage = num1 + num2 + num3 + num4;
	let average = Math.round(totalAverage)
	let text = `Hello, student, your average is ${average}. The letter equivalent is`
	if (average <= 74) {
		console.log(` F`)
	} else if (average >= 75 && average <= 79) {
		console.log(`${text} D`)
	} else if (average >= 80 && average <= 84) {
		console.log(`${text} C`)
	} else if (average >= 85 && average <= 89) {
		console.log(`${text} B`)
	} else if (average >= 90 && average <= 95) {
		console.log(`${text} A`)
	}  else {
		console.log(`${text} A+`)
	}  
}

checkAverage(25, 25, 25, 19.5)

try{
    module.exports = {
        login: typeof login !== 'undefined' ? login : null,
        checkAverage: typeof checkAverage !== 'undefined' ? checkAverage : null
    }
} catch(err){

}

