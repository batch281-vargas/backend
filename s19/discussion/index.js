let numA = -1;

if (numA < 0) {
	console.log("is less than 0")
}

console.log(numA < 0)


let city = "New York";

if(city === "New York") {
	console.log(`Welcome to ${city}!`)
}


let numB = 1;

if (numA > 0) {
	console.log('Hello')
} else if (numB > 1) {
	console.log('W')
}


if(numA > 0) {
	console.log('Hello')
} else if (numB === 0) {
	console.log('World')
} else {
	console.log('Again')
}


// let age = parseInt(prompt("Enter age: "));
// console.log(age);

// if (age <= 18 ) {
// 	console.log(`Minor`)
// } else {
// 	console.log(`Old`)
// }


function heightFunction(hValue) {
	if (hValue <= 150) {
		console.log(`Did not pass the min height requirement.`)
	} else if (hValue > 150){
		console.log(`Passed the min height requirement.`)
	}
} 

heightFunction(200);


function DTI(windSpeed) {
	if (windSpeed < 30) {
		return `Not a Typhoon`
	} else if (windSpeed <=61) {
		return  `Storm`
	} else if (windSpeed >=89 && windSpeed <=177) {
		return `Servere storm`
	} else {
		return `Typhoon`
	}
}


let msg = DTI(10)
console.log(msg)



// Truthy and Falsy

/*
	In JS a truthy value is a value that is considered true when encountered in a boolean context.

	Falsy value:
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN
*/	

// Truthy Examples:

let word = "true"
if(word){
	console.log("Truthy");
};

if(true) {
	console.log("Truthy");
};

if(1) {
	console.log("Truthy");
};


// Falsy Examples:
if(false) {
	console.log("Falsy");
};

if(0) {
	console.log("Falsy");
};

if(undefined) {
	console.log("Falsy");
};

if(null) {
	console.log("Falsy");
};

if(-0) {
	console.log("Falsy");
};

if(NaN) {
	console.log("Falsy");
};


let name;


function isOfLegalAge () {
	name = 'john';
	return "You are the legal age limit"
} 


function isUnderAge () {
	name = 'jane';
	return 'Under age'
}


// let yourAge = parseInt(prompt('What is your age?'));


// let legalAge = (yourAge > 18 ) ? isOfLegalAge() : isUnderAge();
// console.log(`Result of ternary operator in function: ${yourAge}, ${name}`);

// SWITCH


// let day = prompt('Day?').toLowerCase();

// switch(day) {
// 	case 'monday':
// 		console.log('red')
// 		break;
// 	case 'tuesday':
// 		console.log('red')
// 		break;
// 	case 'wednesday':
// 		console.log('red')
// 		break;
// 	case 'thursday':
// 		console.log('red')
// 		break;
// 	case 'friday':
// 		console.log('red')
// 		break;
// 	case 'saturday':
// 		console.log('red')
// 		break;
// 	case 'sunday':
// 		console.log('red')
// 		break;
// 	default:
// 		console.log("invalid");
// 		break;
// }


function showIntensity(windSpeed) {
	try  {
		alert(DTI(windSpeed))
		
	} catch (error) {
		console.log(typeof error);
		console.log(error)
		console.warn(error.message)
	} finally {
		alert('New alert')
	}
}


showIntensity(90)