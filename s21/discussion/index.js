// Array traversal

let grades = [98.5, 94.5, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', "Redfox", 'Gateway', ' Toshiba', 'Fujitsu'];
let task = [ 'drink html', 'eat javascript', 'inhale css', 'bake sass'];


// Creating an array with values from variables

console.log(`${grades} ${computerBrands}`)

let city1 = 'Tokyo';
let city2 = 'Manila';
let city3 = 'Mumbai';


let cities = [city1, city2, city3];

console.log(...task)
console.log(...cities)


console.log(task)
console.log(cities)


// Array length property

console.log("Array length");

console.log(task[0].length)

let fullName = "Niel Vargas"

console.log(fullName.length)


task.length = task.length - 1;

console.log(task.length);

console.log(task)



cities.length--;
console.log(cities)


fullName.length = fullName.length -1
console.log(fullName.length)
console.log(fullName)



let theB = ['john', 'Paul', ' Ringo', 'George'];
theB.length++;
console.log(theB)



let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];

console.log(lakersLegends[1])
console.log(lakersLegends[3])


let currentLaker = lakersLegends[2]

console.log(currentLaker)



let lastIndex = lakersLegends.length - 1;

console.log('===========');
console.log(lakersLegends[lastIndex]);


console.log('===========');

console.log(lakersLegends[lakersLegends.length - 1]);



let newArr = [];

console.log(newArr[0]);

newArr[0] = "Test";
console.log(newArr)


let compBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', "Redfox", 'Gateway', ' Toshiba', 'Fujitsu'];

for(i= 0; i < compBrands.length; i++) {
	console.log(compBrands[i])
}


let numArr = [5, 12, 30 ,46 ,40]

for (let i = 0; i < numArr.length; i++) {
	if (numArr[i] % 5 === 0) {
		console.log(`${numArr[i]}divisible by 5`)
	} else {
		console.log(`${numArr[i]} not divisible by 5`)
	}
}



// Multidimensional Array


let twoDim = [['elem1', 'elem2'], ['elem3', 'elem4']];

console.log(twoDim[1][0]);

console.log(twoDim[1].length);


