//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]
// async function getSingleToDo(){

//     return await (

//        //add fetch here.
       
//        fetch('<urlSample>')
//        .then((response) => response.json())
//        .then((json) => json)
   
   
//    );

// }



// Getting all to do list item
async function getAllToDo(){
    return await (

      fetch('https://jsonplaceholder.typicode.com/todos')
      .then((response) => response.json())
      .then((json) => {
         const titles = json.map((item) => item.title);
         // console.log(titles); 
         return titles;
      })

  );

}

getAllToDo().then((titles) => {
   console.log(titles); 
});

console.log('=======================================')

// [Section] Getting a specific to do list item

async function getSpecificToDo(){
   
      try {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos');
    if (!response.ok) {
      throw new Error('Failed to fetch data');
    }
    const data = await response.json();
    const firstElement = data[0]; // Retrieve the first element of the array
    return firstElement;
  } catch (error) {
    console.error(error);
    // Handle error accordingly
  }

}

(async () => {
  const firstElement = await getSpecificToDo();
  console.log(firstElement);
})();



console.log('=======================================')


// // [Section] Creating a to do list item using POST method
async function createTodo(){
   
  try {
    const todoData = {
      title: "New Todo",
      completed: false
    };

    const response = await fetch('https://jsonplaceholder.typicode.com/todos', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(todoData)
    });


    if (!response.ok) {
      throw new Error('Failed to create todo');
    }

    const createdTodo = await response.json();
    return createdTodo;
  } catch (error) {
    console.error(error);
    // Handle error accordingly
  }

}

(async () => {
  const createdTodos = await createTodo();
  console.log(createdTodos);
})();


console.log('=======================================')

// // [Section] Updating a to do list item using PUT method
async function updateToDo(todoId, updatedData){
   
  try {
    const response = await fetch(`https://jsonplaceholder.typicode.com/todos/${todoId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(updatedData)
    });

    if (!response.ok) {
      throw new Error('Failed to update todo');
    }

    const updatedTodo = await response.json();
    return updatedTodo;
  } catch (error) {
    console.error(error);
    // Handle error accordingly
  }

}

(async () => {
  const todoId = 1; // ID of the todo item to update
  const updatedData = {
    title: 'Updated Todo using Put',
    completed: true,
    userId: 1
  };
  
  const updatedTodo = await updateToDo(todoId, updatedData);
  console.log(updatedTodo);
})();

console.log('=======================================')
// // [Section] Deleting a to do list item
async function deleteToDo(todoId){
   
  try {
    const response = await fetch(`https://jsonplaceholder.typicode.com/todos/${todoId}`, {
      method: 'DELETE'
    });

    if (!response.ok) {
      throw new Error('Failed to delete todo');
    }

    return 'Todo deleted successfully';
  } catch (error) {
    console.error(error);
    // Handle error accordingly
  }

}

(async () => {
  const todoId = 2; // ID of the todo item to delete
  const deleteMessage = await deleteToDo(todoId);
  console.log(deleteMessage);
})();




//Do not modify
//For exporting to test.js
try{
   module.exports = {
       getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
       getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
       getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
       createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
       updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
       deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
   }
} catch(err){

}
