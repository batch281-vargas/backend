// FETCH



console.log(fetch('https://jsonplaceholder.typicode.com/posts'))

// fetch('https://jsonplaceholder.typicode.com/posts')
//   .then(response => {
//     if (!response.ok) {
//       throw new Error('Network response was not OK');
//     }
//     return response.json();
//   })
//   .then(data => {
//     // Handle the data from the response
//     console.log(data);
//   })
//   .catch(error => {
//     // Handle any errors that occurred during the request
//     console.error('Error:', error);
//   });

  // consol.log()


// fetch('https://jsonplaceholder.typicode.com/posts')
// .then((res) => res.json())
// .then((json)=> console.log(json));

async function fetchData () {
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
	console.log(result);

	console.log(typeof result);

	console.log(result.body)

	let json = await result.json();
	console.log(json)
}

fetchData();


// PROCESS a GET REQUEST USING POSTMAN

/*
	postman:
	url: https://jsonplaceholder.typicode.com/posts
	method: GET
	
*/


fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((res) => res.json())
.then((json)=> console.log(json))



// crea A NEW POST FOLLOWING THE Res api ()create / POST/:ID, POST



fetch('https://jsonplaceholder.typicode.com/posts', {
	//Sets the method of the request objec to post 
	//DEFAULT METHOD IS GET
	method: 'POST',
	// Sets the method of the request of object to  "POST" following the REST API
	// Specified theat the content will be in a JSON structure
	headers : {
		'Content-Type': 'application/json',

	},
	body: JSON.stringify({
		title: 'Updated POST',
		body: 'Hello WORLD',
		userID: 1
	})
})
.then((res) => res.json())
.then((json) => console.log(json));



// PUT is used to uodate the whole object
// PATCH is used to update single/several properties


fetch('https://jsonplaceholder.typicode.com/posts', {
	//Sets the method of the request objec to post 
	//DEFAULT METHOD IS GET
	method: 'PATCH',
	// Sets the method of the request of object to  "POST" following the REST API
	// Specified theat the content will be in a JSON structure
	headers : {
		'Content-Type': 'application/json',

	},
	body: JSON.stringify({
		title: 'CORRECTED USING PATCH',
	})
})
.then((res) => res.json())
.then((json) => console.log(json));



fetch('https://jsonplaceholder.typicode.com/posts/1', {
	// DELETE METHOD
	method: 'DELETE',
})


//filtering posts

fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((res)=> res.json())
.then((json) => console.log(json));



// Retrieving nested/rellated comments to posts

// REST API(retrieve, / posts/:id/comments)


fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((res)=> res.json())
.then((json) => console.log(json));