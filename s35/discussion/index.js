const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;


// Mongo DB Connection

mongoose.connect("mongodb+srv://nielvargas96:MbQPGFGeZkjPBnXt@wdc028-course-booking.wixug5i.mongodb.net/b281_to-do?retryWrites=true&w=majority",

	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)

// Set notification for connection success or failure
let db = mongoose.connection;

// connection error occured, ooutput in console
db.on("error", console.error.bind(console, "connection error"));

// If the connection is sucessful, output in the console

db.once("open", () => console.log("We're connected to the cloud database"))


// Creation of todo list routes

// Allows the app

//======================================================================================//

app.use(express.json());
// Allows the app to read data 
app.use(express.urlencoded({extended:true}));

//======================================================================================//

// Mongoose Schema
const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
})


// Models [Tasks models]
const Task = mongoose.model("Task", taskSchema);

// Creating a new task
app.post("/tasks", (req, res)=> {

	// 2nd step
	Task.findOne({name : req.body.name}).then((result, err)=> {
		// 1st step
	// If a document was found and documents name matches the information from the client
		if(result !== null && result.name === req.body.name) {
			// Return a message to the client/Postman
			return res.send(`Duplicate task found!`)
		}
		// If no document was found
		else {
			// Create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			});

			// Save it
			newTask.save().then((saveTask, err)=> {
				// If there is an error

				if(err) {
					return console.error(err)
				} else {
					return res.status(201).send("New task created.")
				}
			})
		}
	})

})

// Get all the tasks

app.get("/tasks", (req, res) => {
	Task.find({}).then((result, err) => {
		if (err) {
			return console.error(err)
		} else {
			return res.status(200).json({
				data : result
			})
		}
	})
})



// ACTIVITY

// User Schema
const userSchema = new mongoose.Schema({
	username : String,
	password: String,
	status : {
		type : String,
		default : "pending"
	}
})

// Models [User models]
const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res)=> {
	let newUser = new User({
		username : req.body.username,
		password : req.body.password
	})

	newUser.save().then((savedUser) => {
    return  res.status(201).send("New user registered.")
	}).catch((err) => {
	    console.error(err);
	});
});


// Listen to port

app.listen(port, () => console.log(`Server is running at port ${port}`));