

//CRUD


//insert one
db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Dela Cruz",
	"age": 21,
	"contact": {
		"phone": "1231231",
		"email": "jane@mail.com"
	}
	"course": ["CSS", "JS", "HTML"],
	"department": "none"
});


//Insert Many
db.users.insertMany([
	{
		"firstName": "John",
		"lastName": "Dela Cruz",
		"age": 21,
		"contact": {
			"phone": "1231231",
			"email": "jane@mail.com"
		},
		"course": ["CSS", "JS", "PHP"],
		"department": "none"
	},
	{
		"firstName": "Jay",
		"lastName": "Dela Cruz",
		"age": 21,
		"contact": {
			"phone": "1231231",
			"email": "jane@mail.com"
		},
		"course": ["CSS", "JS", "REACT"],
		"department": "none"
	}
]);


// Find
db.users.find()

// Retrieve single documents
db.users.find({"_id" : ObjectId("64661786183fb0e72e5f64c8")})
db.users.find({"firstName": "John"})

// Retriev multitple parameters
db.users.find({"firstName": "John","lastName": "Dela Cruz"});


// Creating a document to update
// UPDATE

db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"contact": {
		"phone": "0000000",
		"email": "test@mail.com"
	}
	"course": [],
	"department": "none"
});
		

db.users.updateOne(
	{"firstName": "Test"},
	{
		$set : {
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 0,
			"contact": {
				"phone": "123456",
				"email": "bill@mail.com"
			},
			"course": ["PHP", "Laravel", "JS"],
			"department": "Operations",
			"Status": "active"
			}
	}
);

//Update to many

db.users.updateOne(
	{"firstName": "Bill"},
	{
		$set : {
			"status": "active"
			}
	}
);


//Update to many

db.users.updateMany (
	{"department": "none"},
	{
		$set : {
			"department": "HR"
			}
	}
);


// Replace one -- status will remove 
db.users.replaceOne (
	{"lastName": "Gates"},
	{
		"firstName": "Bill",
		"lastName": "Gates",
		"age": 0,
		"contact": {
			"phone": "123456",
			"email": "bill@mail.com"
		},
		"course": ["PHP", "Laravel", "JS"],
		"department": "Operations"
	}
);

// DELETE

db.users.insertOne(
{
	"firstName": "test"
})


// Deleting a single document


db.users.deleteOne({
	"firstName": "test"
})


// Query embbed
db.users.find({
	"contact": {"phone": "123456","email": "bill@mail.com"
}})

db.users.find({
    "contact.phone": "123456"
});
