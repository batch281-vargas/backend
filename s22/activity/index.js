let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

let register = function(user) {
    if (registeredUsers.includes(user)) {
        alert("Registration failed. Username already exists!")
    } else {
      registeredUsers.push(user);
      alert("Thank you for registering!")
    }
}

// register('')

let addFriend = function(user) {
     if (registeredUsers.includes(user)) {
        friendsList.push(user)
        alert(`You have added ${friendsList[friendsList.length - 1]} as a friend!`)
    } else {
        alert(`User not found.`)
    }
}


let displayFriends = function (user) {
    if (friendsList.length === 0 ) {
        alert(`You currently have ${friendsList.length} friends. Add one first.`)
    } else {
        alert(`You currently have ${friendsList.join(", ").toString()} friends.`)

        //  friendsList.forEach(function(user) {
        //     console.log( `You currently have ${user} friends.`)
        // })

    }
}


let displayNumberOfFriends = function (user) {
    if (friendsList.length === 0 ) {
        alert(`You currently have ${friendsList.length} friends. Add one first.`)
    } else {
        alert(`You currently have ${friendsList.length} friends.`)
    }
}

let deleteFriend = function (user) {
    if (friendsList.length === 0 ) {
        alert(`You currently have ${friendsList.length} friends. Add one first.`)
    } else {
        friendsList.pop()
        alert(`You currently have ${friendsList} friends.`)
    }
}


let deleteSpecificFriends = function(user) {
    if (friendsList.includes(user) === true) {
        let index = friendsList.indexOf(user);
        friendsList.splice(index, 1)
    } else {
        console.log(`${user} not found.`)
    }
}