const express = require("express");


const app = express();
const port = 4000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));



app.get("/home", (req, res)=> {
  res.send("Welcome to the homepage!")
})


let users = [];

// add mock user
app.post("/users", (req, res)=> { 
  users.push(req.body)
  res.send(users)
})

// get  users
app.get("/users", (req, res)=> { 
  res.send(users)
})

let message; 

// app.delete("/users/:id", (req, res) => {
//     const userId = req.params.id;



//     res.send(`User ${userId} has been deleted.`);
// });


// let users = [
//         {
//             "id": 1,
//             "username": "John",
//             "password": "123"
//         },
//         {
//             "id": 2,
//             "username": "Jona",
//             "password": "123"
//         }
//     ]

app.delete("/users/:id", (req, res) => {
  const userId = req.params.id;
  const usernameToDelete = req.body.username;
  let message;

  for (let i = 0; i < users.length; i++) {
    if (users[i].username === usernameToDelete) {
      users.splice(i, 1);
      message = `User with username ${usernameToDelete} has been deleted.`;
      break;
    }
  }

  if (!message) {
    message = `User with username ${usernameToDelete} not found.`;
  }

  res.send(message);
});





if(require.main === module) {
  app.listen(port, ()=> console.log(`Server is running on port ${port}`))
}

module.exports = app;