// Use the require directive to load the express module/package
// A module is a software components or part of a program that containes one or more routines
// This is used to get the contents of tje express package to be used by our applicaton
// It also allows us to acces methods and functions that will allow us to easy create a server
const express = require("express")


// Create an applicatio using express
// This creates an express application and stores this in consant call app
// In laymants terms app is our sercer
const app = express();
console.log(app)


// For our application server to run, we need a port to listen
const port = 4000;

// Setup for allowint the server to handle data from requests
// Allows your app to read JSON data

// Methods used from expressJS are middle wards
// Middlewards is software the provides common services and capabalities to applications outside of whats offered by the operating system
// API manangement is one of the common applications of middlewares
app.use(express.json())


// Allows your app to read data from forms
// By default, information received from the URL can only be received as a string or an array
// By applying the option of "extended:true" this allows us to receive informaition in other data types such as an objecy which we will use throughout our applicaiton
app.use(express.urlencoded({extended:true}));

// [Section] Rooutes
// Express has methods correspondng to each HTTP method
// This route expects to receive a GET request at the base URI of "/"
// THe full base URI for our ocal application for this route will be at "http://localhost:4000"


app.get("/", (req, res)=> {
	// Once the route is accessed it will send a string response container "Hello"
	// res.send uses the express JS modules's method instead to send a response back to the client
	res.send('Hello World')
})

app.get("/hello", (req, res)=> {
	res.send("Hello from hello endpoinst")
})

app.get("/hello", (req, res)=> {
	res.send(`Hello here ${req.body.firstName} ${req.body.lastName}!`)

})

// This serve as our mock database
let users = [];

// This route expects to recieve a POST request at URI "/signup"
app.post("/signup", (req, res)=> {
	console.log(req.body);
	console.log(users);


	// If contents of the "request body" with the property "username and "password" is not empty

	if(req.body.username !== '' && req.body.password !== '') {
		users.push(req.body)
		res.send(`User ${req.body.username} sucessfully registered!`)		
	} else {
		res.send('Please input BOTH username and password')
	}
})


app.put("/change-password", (req, res)=> {
	
	let message;

	for(let i = 0; i<users.length; i++) {
		// If the usersname provided in the client/postman and the username of the current object in loop is the same
		if(req.body.username == users[i].username) {
			// Changes the message to be sent back
			users[i].password = req.body.password

			// Changes the message to be sent back by the response

			message = `Users ${req.body.username}'s password has been updated.`

			console.log(users)
			//
			break;
		} else {
			// Change
			message = `Users does not exist.`
		}
	}

	res.send(message)
})




if(require.main === module) {
	app.listen(port, ()=> console.log(`Server is running at port ${port}`))
}

module.exports = app;	