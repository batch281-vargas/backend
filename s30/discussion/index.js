db.fruits.insertMany([
	{	
		"name": "Apple",
		"color": "Red",
		"stock": 20,
		"price": 40,
		"supplier_id": 1,
		"onSale": true,
		"origin": ["Philippines", "US"]
	},
	{	
		"name": "Banana",
		"color": "Yellow",
		"stock": 15,
		"price": 20,
		"supplier_id": 2,
		"onSale": true,
		"origin": ["Philippines", "Ecuador"]
	},
	{	
		"name": "Kiwi",
		"color": "Green",
		"stock": 25,
		"price": 50,
		"supplier_id": 1,
		"onSale": true,
		"origin": ["US", "China"]
	},
	{
		"name": "Mango",
		"color": "Yellow",
		"stock": 10,
		"price": 120,
		"supplier_id" : 2,
		"onSale": false,
		"origin": ["Philippines", "India"]
	}
]);


// MONGO DB AGGREGATION

// $match - used to pass documents that meet specified condtitions

/*
	Syntax
	- { $match: {field:value} }
	
	-$group - used to group elements together and field- value pairs using data from the group elements

	 -Syntax
	 	db.collectionName.aggregate([
	 	{ $match: { field} }, 
	 	{ $group: { _id: "$filedB "},{ result: { operation } } }
	 	])
*/

db.fruits.aggregate([
 	{ $match: { "onSale": true }},
 	{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
]);


db.fruits.aggregate([
	{ $match: { "onSale": true }},
	{ $group: { _id: "$supplier_id", $total: { $sum: "$stock" }} }
]);


// With sorting
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $sort: { _id: 1 } }
]);


// Field Projections with aggregation

/*

$project - used when aggregation data to include/exclude field from the returned resuls
Syntax
	- {project : { field: 1/0 }}

*/

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $project: { _id: 1 } }
]);


db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $project: { _id: 0 } }
]);


db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $sort: { _id: 1 } },
  { $project: { _id: 1 } }
]);



// Aggregating results based on array fields.

/*

	$unwind -  deconstruct

*/


db.fruits.aggregate([
	{$unwind: "$origin"}
])


// Displays fruit documents by theur origin and the kinds of fruits that are supplied

db.fruits.aggregate([
	{ $unwind: "$origin" },
	{ $group: { _id: "$origin" , kinds: { $sum: 1 } }}
])


var owner = ObjectId();


db.owners.insertOne({
	_id: owner,
	namae: "John Smith",
	contact: "0912345678"
})


db.suppliers.insertOne({
	name: "ABC fruits",
	contact: "09123456678",
	owner_id: ObjectId("646b5c142ae21bab95b0792f")
})


// One to few relationship

db.suppliers.insertOne({
	name: "DEF Fruits",
	contact: "0912344566",
	addresses: [
		  { street : "123 San Jose St", city: "Manila" },
		  { street : "456 Gil Puyat", city: "Makati" }
		]
});



// One to many

var supplier = ObjectId();
var branch1 = ObjectId();
var branch2 = ObjectId();

db.suppliers.insertOne({
	_id: supplier,
	name: "GHI Fruits",
	contact: "09123445693",
	branches: [
		branch1
		]
})

db.branches.insertOne({
	_id: branch1,
	name: "BF Homes",
	address: "123 Bonifactio street",
	city: "Paranaque",
	supplier_id: ObjectId("646b61d42ae21bab95b07932")
})

db.branches.insertOne({
	_id: branch2,
	name: "BF Homes",
	address: "321 Rizal street",
	city: "Makati",
	supplier_id: ObjectId("646b61d42ae21bab95b07932")
})

