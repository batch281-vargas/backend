//JSON as objects


// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// JASON ARRAYS

// "dogs": [
// 		{"name": "Istoy", 
// 		"age":1,
// 		"breed": "Belgian Malinois"},
// 		{"name": "Soffie", 
// 		"age":1,
// 		"breed": "Shitzu"},
// 		{"name": "Borjak", 
// 		"age":1,
// 		"breed": "Belgian Groenendale"}
// 	];

// JSON Methods

// Convert data into stringified JSON


// let batchesArr = [{batchName: 'Batch X'}, {batchName: 'Batch Y'}];

// console.log('Result from stringify method:');
// console.log(JSON.stringify(batchesArr));


// let data = JSON.stringify({
// 	name: 'John',
// 	age: 31,
// 	address: {
// 		city: 'Mnl',
// 		country: 'Ph'
// 	}
// })

// console.log(data)


// let firstName = prompt('Fname?');
// let lastName = prompt('Lname?');
// let age = prompt('age?');
// let address = {
// 	city: prompt('city?'),
// 	country: prompt('countrt?')
// }


// let otherData = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address: address
// })


// console.log(otherData)


// let firstName = prompt('Fname?');
// let lastName = prompt('Lname?');
// let age = prompt('age?');
// let address = {
// 	city: prompt('city?'),
// 	country: prompt('countrt?')
// }


// let otherData = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address: address
// })


// console.log(otherData)

// let brand = prompt('brand?');
// let type = prompt('type?');
// let year = prompt('year?');

// let carData = {
// 	brand: brand,
// 	type: type,
// 	year: year
// }


// console.log(carData)

// Stringified JSON ti JS objects

// let batchesJSON = `[{Batch X}, {Batch Y}, {Batch Y}],[{Batch X}, {Batch Y}, {Batch Y}]`;
let batchesJSON = '{"name":"John", "birth":"1986-12-14", "city":"New York"}';

console.log(`Parse method`);
console.log(JSON.parse(batchesJSON))