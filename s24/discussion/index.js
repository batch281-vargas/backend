const firstNum = 8 ** 2;
console.log(firstNum);


const secondNum = Math.pow(8, 2);
console.log(secondNum);


let name = 'John';

console.log(`Hi my name is ${name}`);


// Array destructring

const fullName = ['Juan', 'Dela', 'Cruz'];


// destructure

const [firstName, middleName, lastName] = fullName;

console.log(`Hello ${firstName}, ${middleName} ${lastName}`);


//Object Destructuring

const person = {
	p_name: 'Jane',
	p_maiden: 'Castro',
	p_surname: ' Dela Cruz'
} 

//Object destructure

const {p_name, p_maiden, p_surname} = person;

console.log(`${p_name} ${p_maiden} ${p_surname}`)



//Arrow functions alternative syntax to traditional functions

const hello = () => {
	console.log(`Hello world`)
}

hello();


const printFullname = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`)
}

printFullname('test', 'test1', 'test2')



const students = ['john', 'jane', 'joy'];

students.forEach(function(student) {
	console.log(`${student}`)
})


students.forEach((student)=> {
	console.log(`${student} iterate with arrow functions`)
})


const add = (x, y) => (x + y);

let total = add(5, 5)
console.log(total)


//Default function argument value

const greet = (name = 'Users') => {
	return `Hi, ${name}!`
}

console.log(greet())



// Class based object blueprints


// Creating a class


class Car {
	constructor(brand, name , year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// Initantiate an object

let myCar = new Car('test', 'test', 'test');

console.log(myCar)


// Values of properties assigned after instantiation of an object

myCar.brand = 'Ford';
myCar.name = 'Ranger Raptor';
myCar.year = '2021';

console.log(myCar);





























